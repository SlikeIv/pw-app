window.onload = function() {

  function get_user(name , callback){
    var requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };

    fetch(`/catalog/user-list?name=${name}`, requestOptions)
      .then(response => response.json())
      .then(result => {
        return callback(result)
      })
      .catch(error => console.log('error', error));
  }


  function click_on_suggestion(e){
      var user_id = e.target.getAttribute("data-user_id")
      var user_name = e.target.innerHTML
      hiddenInput.value = user_id
      var operationUserInput = document.querySelector('[name="operation_user"]')
      operationUserInput.value = user_name

      suggestionListElem.innerHTML = ""
  }


  function show_suggestion(users){
      var content = ''
      suggestionListElem.innerHTML = ""
      users.forEach((user, i) => {

        let item_elem = document.createElement('li');
        item_elem.innerHTML = `<li class="border list-none rounded-sm px-3 py-3" data-user_id="${user.id}">${user.name}</li>`;
        item_elem.addEventListener('click' , function(e){
            click_on_suggestion(e)
        })
        suggestionListElem.appendChild(item_elem);
      });

  }





  var suggestionListElem = document.querySelector('#suggestion-name')
  var hiddenInput = document.querySelector('[name="operation_user_id"]')
  var inputElem = document.querySelector('[name="operation_user"]')
  inputElem.addEventListener("blur", function() {

      if(!hiddenInput.value || hiddenInput.value.length === 0 ) {
        inputElem.value = ''
      }
      

  })






  inputElem.addEventListener('input',  function(e) {

      var name = e.target.value
      hiddenInput.value = ''

      if(name.length >=2 ) {
        get_user(name, function(result) {

            if(result.hasOwnProperty("users") && Array.isArray(result.users)) {

              if(result.users.length == 1) {

                hiddenInput.value = result.users[0].id
                show_suggestion(result.users)
              }else{
                  show_suggestion(result.users)
              }

            }
        })
      }
  })

}
