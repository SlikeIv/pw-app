<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');
//



Route::group(['middleware' => 'auth'], function(){

  Route::group(['middleware' => 'check_ban'], function(){
          // for users
        Route::get('/', 'App\Http\Controllers\TransactionController@transaction_history')->name("home");
        Route::get('/transaction/history', 'App\Http\Controllers\TransactionController@transaction_history')->name("transaction_history");
        Route::get('/transfer/create', 'App\Http\Controllers\TransactionController@transfer_create')->name("transfer_create");
        Route::get('/transfer/copy/{transaction_id}', 'App\Http\Controllers\TransactionController@transfer_copy')->name("transfer_copy");
        Route::post('/transfer/transfering', 'App\Http\Controllers\TransactionController@transfering')->name("transfering");
        Route::get('/transfer/result', 'App\Http\Controllers\TransactionController@transfer_result')->name("transfer_result");
        Route::get('/catalog/user-list', 'App\Http\Controllers\CatalogController@user_list')->name("user_list");
   });

  //admin control panel
  Route::group(['prefix' => 'admin'], function(){

        Route::group(['middleware' => 'can:isAdmin'], function(){
        //admin access
        Route::resource('user', 'App\Http\Controllers\AdminControllers\UserController');
        Route::resource('transaction', 'App\Http\Controllers\AdminControllers\TransactionController');

      });
  });
});


require __DIR__.'/auth.php';
