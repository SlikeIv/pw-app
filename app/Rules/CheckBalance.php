<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\Models\UserData;


class CheckBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
          $user_id = Auth::id();
          $user_data = \App\Models\UserData::where('user_id', $user_id)->first();
          if(!empty($user_data)){
            if( $user_data->current_balance >= $value) {
              return true;
            }
          }
          return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'There are not enough funds on your balance to transfer the specified amount';
    }
}
