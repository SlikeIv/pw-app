<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Rules\CheckBalance;


class CreateTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'transfer_amount' => ['required', 'min:0.01', 'max:1000000', "regex:/^\d+(.\d{1,2})?$/", new CheckBalance],
          'operation_user' => ['required', 'string'],
          'operation_user_id' => ['required_with:operation_user', 'integer', 'exists:users,id'],
          'request_uid' => ['required', 'uuid', 'unique:transactions'],
        ];
    }


    public function messages()
    {
        return [
            'operation_user.integer' => 'User is not found',
            'operation_user.exists' => 'User is not found',
            'operation_user_id.required_with' => 'User is not found!',
          'request_uid.*' => 'The transfer of funds has already been processed',
        ];
    }
}
