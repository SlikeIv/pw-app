<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckBannedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      if (Auth::check()){
          if (Auth::user()->is_banned() === true)
          {
              Auth::logout();
              return redirect()->to('/login')->with('message', 'Your account is banned!');
          }
      }
    return $next($request);
    }
}
