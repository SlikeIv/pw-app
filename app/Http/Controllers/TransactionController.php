<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Redirect;

use App\Helpers\Filters\TransactionFilter;
use App\Http\Requests\CreateTransferRequest;
use App\Rules\CheckExistTransactionByUser;
use App\Models\Transaction;
use App\Models\User;
use App\Helpers\TransferOperation\TransferHandler;
use App\Helpers\TransferOperation\TransferTransaction;

class TransactionController extends Controller
{

    public function transaction_history(Request $request)
    {
        $rows = TransactionFilter::get_rows($request);

        return view('transaction.transaction_history', compact(
          'rows',
          'request'
        ));
    }


    public function transfer_create(Request $request)
    {
        $request_uid = Transaction::generate_request_uid();
        return view('transaction.transfer_create', compact(
          'request',
          "request_uid",
        ));
    }

    public function transfer_copy(Request $request, $transaction_id)
    {
        $user = Auth::user();


        //$transaction_id = $request->input('transaction_id');
        $transaction = Transaction::where(["id" => $transaction_id, "user_id" => $user->id  , "operation_type" => "debiting"])->with(["operation_user_rel"])->firstOrFail();

        return Redirect::route('transfer_create')->withInput(["operation_user" => $transaction->operation_user_rel->name, "operation_user_id" =>$transaction->operation_user, "transfer_amount" => $transaction->amount ]);

    }



    public function transfering(CreateTransferRequest $request)
    {

      $user = Auth::user();
      $operation_user = User::find($request->get("operation_user_id"));
      $amount =  $request->get("transfer_amount");
      $request_uid =  $request->get("request_uid");

      $transaction_operation = new TransferTransaction();
      $transfer = new TransferHandler($transaction_operation);
      $sender_transaction = $transfer->transfer($user, $operation_user, $amount, $request_uid);

      return redirect()->route('transfer_result', ['transaction_id' =>  $sender_transaction->id]);

    }


    public function transfer_result(Request $request)
    {
        $validator =  Validator::make($request->all(), [ 'transaction_id' => ['required', 'integer',  new  CheckExistTransactionByUser] ]);

        if($validator->fails()){
            return response('Transaction not found', 400);
        }

        $user_id = Auth::id();
        $transaction = Transaction::with('operation_user_rel')->where("id", $request->transaction_id)->where('user_id', $user_id)->first();


        return view('transaction.transfer_result', compact(
          "transaction"
        ));



    }

}
