<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

use App\Helpers\Filters\TransactionAdminFilter;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rows = TransactionAdminFilter::get_rows($request);
        return view('admin_views.transaction.index', compact(
          'rows',
          'request'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::where(["id" => $id])->with(["user_rel", "operation_user_rel"])->firstOrFail();
        return view('admin_views.transaction.details', ['transaction' => $transaction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::where(["id" => $id])->with(["user_rel", "operation_user_rel"])->firstOrFail();
        return view('admin_views.transaction.edit', ["transaction" => $transaction]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $request->validate([
          'amount'=> ['required', 'min:0.01', 'max:1000000', "regex:/^\d+(.\d{1,2})?$/"],
          'fail_message'=>'string|max:100',
        ]);

       $transaction->amount =  $request->get('amount');
       $transaction->fail_message = $request->get('fail_message');
       $transaction->save();
       return redirect()->route('transaction.index', [])->with('message', 'Transaction updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
