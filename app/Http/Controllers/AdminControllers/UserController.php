<?php

namespace App\Http\Controllers\AdminControllers;

use \Validator;
use App\Http\Controllers\Controller;
use \Redirect;
use \Hash;
use Illuminate\Http\Request;

use App\Models\User;
use App\Helpers\Filters\UserFilter;

class UserController extends Controller
{


  // public function store(Request $request)
  // {
  //
  //    $request->flash();
  //
  //    $validator =  Validator::make($request->all(),[
  //      'name'=>'required',
  //      'email'=>'required|email',
  //      'status'=>'required|integer',
  //      'password'         => 'sometimes|required|min:6|max:14',
  //      'password_confirm' => 'required_with:password|same:password|min:6|max:14'
  //    ] );
  //
  //
  //    if ($validator->fails()) {
  //       return Redirect::to('user/create')
  //           ->withErrors($validator)
  //           ->withInput($request->input());
  //    }
  //
  //     $user = new User([
  //         'name' => $request->get('name'),
  //         'email' => $request->get('email'),
  //         'status' => (int) $request->get('status'),
  //         "password" => Hash::make($request->get('password')),
  //     ]);
  //
  //     $user->save();
  //     return redirect()->route('user.index', [])->with('success', 'User saved!');
  // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $users = UserFilter::get_rows($request);
       $roles = User::get_roles_list();
       $statuses = User::$user_status;
       return view('admin_views.user.index', compact('users' , 'request',  'roles', 'statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin_views.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     //
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::where(["id" => $id])->with("userdata__rel")->firstOrFail();
      return view('admin_views.user.profile', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $user = User::where(["id" => $id])->with("userdata__rel")->firstOrFail();

       $roles = User::get_roles_list();
       $statuses = User::$user_status;

       return view('admin_views.user.edit', compact('user', 'roles', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'name'=>'required|unique:users,name,'.$id,
          'email'=>'required|email|unique:users,email,'.$id,
          'status'=>'required|integer',
          'role'=>'required',
          'password' => 'sometimes|nullable|min:6|max:14',
          'password_confirm' => 'required_with:password|same:password|nullable|min:6|max:14'
        ]);


       $user = User::where(["id" => $id])->with("userdata__rel")->firstOrFail();
       $user->name =  $request->get('name');
       $user->email = $request->get('email');
       $user->status = $request->get('status');
       $user->role = $request->get('role');
       if(!empty($request->get('password'))) {
         $user->password = Hash::make($request->get('password'));
       }


       $user->save();
       return redirect()->route('user.index', [])->with('message', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    //
    //     $contact = \App\Models\Contact::find($id);
    //     $contact->delete();
    //
    //     return redirect('/contact')->with('success', 'Contact deleted!');
    // }
}
