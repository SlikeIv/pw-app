<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Validator;

use App\Models\User;

class CatalogController extends Controller
{


    public function user_list(Request $request)
    {
        $validator =  Validator::make($request->all(), [
          'name'=>['required', 'string',  'min:2', 'max:32'] ,
        ], []);

        if($validator->fails()){
            return response()->json([
                'status' => false,
                "error" => 'validation_error',
                "msg" => $validator->errors(),
            ], 422);
        }

        $users = User::select(["id", "name"])->where("name", 'like', '%'.$request->get("name").'%')->where(['status' => 1])->limit(10)->get();
        return  response()->json(['status' => true, 'users' => $users]);
    }
}
