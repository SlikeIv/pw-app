<?php

 namespace App\Helpers\TransferOperation;

interface ITransferOperation
{
    public function execute_transfer(\App\Models\User $sender, \App\Models\User $recipient, float $amount, string $request_uid): bool;
}
