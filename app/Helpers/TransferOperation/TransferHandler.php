<?php

namespace App\Helpers\TransferOperation;

 use App\Helpers\TransferOperation\ITransferOperation;
 use Illuminate\Support\Facades\Log;
/**
 *
 */
class TransferHandler
{

  function __construct(ITransferOperation $transaction_operation)
  {
    $this->transaction_operation = $transaction_operation;
  }


  public function transfer(\App\Models\User $sender, \App\Models\User $recipient, float $amount,  string $request_uid) {

      $result_transaction = $this->transaction_operation->execute_transfer($sender,  $recipient, $amount, $request_uid);
      $transaction_sender = $this->transaction_operation->transaction_sender;
      if($result_transaction === false) {
        Log::warning("fail transfer, message: ". $this->transaction_operation->fail_message. " user_id: ". $sender->id. " transaction_id: ".$transaction_sender->id);
      }

      return $transaction_sender;
  }

}




 ?>
