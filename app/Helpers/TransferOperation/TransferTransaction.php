<?php
/**
 *
 */
 namespace App\Helpers\TransferOperation;

use App\Helpers\TransferOperation\ITransferOperation;
use Illuminate\Support\Facades\DB;

use App\Models\UserData;
use App\Models\Transaction;

class TransferTransaction implements  ITransferOperation
{

  public $fail_message = null;
  public $transaction_sender = null;
  public $transaction_recipient = null;
  public $request_uid = null;


  protected function check_exists_balance ($userdata): float {
      return $userdata->current_balance;
  }


  protected function create_transaction_logs ( string $request_uid , int $sender_id, float $_sender_current_balance,  int $recipient_id, float $_recipient_current_balance, float $amount, bool  $is_transaction_success,  string $fail_message=null): bool {

        // transaction log for sender
        $transaction_sender = new Transaction;
        $transaction_sender->amount = $amount;
        $transaction_sender->is_success = $is_transaction_success;
        $transaction_sender->operation_type = "debiting";
        $transaction_sender->request_uid = $request_uid;
        $transaction_sender->balance_after = $_sender_current_balance;

        if($fail_message) {
          $transaction_sender->fail_message = $fail_message;
        }

        $transaction_sender->user_id = $sender_id;
        $transaction_sender->operation_user = $recipient_id;
        $transaction_sender->save();

        $this->transaction_sender = $transaction_sender;


          // transaction log for recipient
        $transaction_recipient = new Transaction;
        $transaction_recipient->amount = $amount;
        $transaction_recipient->is_success = $is_transaction_success;
        $transaction_recipient->operation_type = "crediting";
        $transaction_recipient->balance_after = $_recipient_current_balance;
        if($fail_message) {
          $transaction_recipient->fail_message = $fail_message;
        }
        $transaction_recipient->user_id = $recipient_id;
        $transaction_recipient->operation_user = $sender_id;
        $transaction_recipient->save();

        $this->transaction_recipient =  $transaction_recipient;

        return true;

  }



  public function execute_transfer(\App\Models\User $sender, \App\Models\User $recipient, float $amount, string $request_uid): bool {


    $this->request_uid = $request_uid;
    $is_transaction_success = true;

    DB::beginTransaction();

    try {


          $_sender_userdata = UserData::where('user_id', $sender->id)->first();
          try {
            $sender_balance = $this->check_exists_balance($_sender_userdata);
          } catch (\Exception $e) {
              $sender_balance = 0;
              throw new \Exception('sender has no balance');
          }


          $_recipient_userdata = UserData::where('user_id', $recipient->id)->first();
          try {
            $recipient_balance = $this->check_exists_balance($_recipient_userdata);
          } catch (\Exception $e) {
              $recipient_balance = 0;
              throw new \Exception('recipient has no balance');
          }


          if ($sender->id === $recipient->id) {
              throw new \Exception('recipient and sender are the same');
          }

          if ($sender_balance < $amount) {
             throw new \Exception('not enough funds for the operation');
          }

          $_sender_userdata->current_balance = $sender_balance - $amount;
          $_sender_userdata->save();


          $_recipient_userdata->current_balance = $recipient_balance + $amount;
          $_recipient_userdata->save();

          $this->create_transaction_logs($this->request_uid, $sender->id, $_sender_userdata->current_balance,  $recipient->id,   $_recipient_userdata->current_balance,  $amount,  $is_transaction_success   );

          DB::commit();

    } catch (\Exception $e) {
        DB::rollback();
        $this->fail_message = $e->getMessage();
        $is_transaction_success = false;
        $this->create_transaction_logs($this->request_uid, $sender->id, $sender_balance,  $recipient->id,  $recipient_balance,  $amount,  $is_transaction_success, $this->fail_message);
    }


    return $is_transaction_success;

  }

}


 ?>
