<?php
namespace App\Helpers\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;


class UserFilter
{

  public static function get_rows(Request $request)
  {

        $user_query = User::query()->with('userdata__rel');

        if (  $request->has('id')  AND   is_numeric($request->input('id')))
        {
             $user_query->where('id', (int) ($request->input('id')));
        }


        if (  $request->has('status')  AND   is_numeric($request->input('status')))
        {
             $user_query->where('status', (int) ($request->input('status')));
        }

        if (  $request->has('role')  AND    !empty($request->input('role')))
        {
             $user_query->where('role',  $request->input('role'));
        }


        if (  $request->has('name')  AND   !empty($request->input('name')) )
        {
             $user_query->where('name', $request->input('name'));
        }

        if (  $request->has('email')   AND   !empty($request->input('email')) )
        {
             $user_query->where('email', 'like',  $request->input('email'));
        }


        if ( $request->has('date_start') && trim($request->input('date_start')) !== '')
        {
            $date_start = date("Y-m-d" ,  strtotime(trim($request->input('date_start'))) );
            $user_query->where('created_at',  '>='  , $date_start. " 00:00:00" );
        }

        if ( $request->has('date_end') && trim($request->input('date_end')) !== '')
        {
            $date_end = date("Y-m-d" ,  strtotime(trim($request->input('date_end'))) );
            $user_query->where('created_at',  '<='  , $date_end. " 23:59:59" );
        }


        // sorting rules
        $field = "id";
        $order = "desc";

        if ($request->has('sort_by')) {
          $sort_by = $request->input('sort_by');

          if($sort_by == 'created_at') {
              $field = "created_at";
              $order = "asc";
          }elseif($sort_by == '-created_at'){
            $field = "created_at";
            $order = "desc";
          }

          if($sort_by == 'id') {
            $field = "id";
            $order = "asc";
          }elseif($sort_by == '-id'){
            $field = "id";
            $order = "desc";
          }

          if($sort_by == 'status') {
            $field = "status";
            $order = "asc";
          }elseif($sort_by == '-status'){
            $field = "status";
            $order = "desc";
          }
        }

        $user_query->orderBy($field, $order);


        return  $user_query->paginate(15)->appends(request()->query());

  }
}




 ?>
