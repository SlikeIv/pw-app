<?php
namespace App\Helpers\Filters;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
/**
 *
 */


class TransactionFilter
{

  public static function get_rows(Request $request)
  {
        $user_id = Auth::id();

        $transaction_query = \App\Models\Transaction::query()->with('operation_user_rel');
        $transaction_query->where('user_id', $user_id);


        if (  $request->has('operation_type')  AND   !empty($request->input('operation_type')) )
        {
             $transaction_query->where('operation_type', trim($request->input('operation_type')));
        }


        if (  $request->has('balance_after')  AND   !empty($request->input('balance_after')) )
        {
             $transaction_query->where('balance_after', trim($request->input('balance_after')));
        }


        if (  $request->has('amount')   AND   !empty($request->input('amount')) )
        {
             $transaction_query->where('amount', trim($request->input('amount')));
        }

        if ($request->has('operation_user')  AND  trim($request->input('operation_user')) !== '')
        {
             //$transaction_query->where('operation_user.name', $request->input('operation_user'));
             $transaction_query->whereHas('operation_user_rel', function($q) use ($request) {
                  $q->where('name', $request->input('operation_user'));
              });
        }


        if ( $request->has('created_at') && trim($request->input('created_at')) !== '')
        {
            $date = date(  "Y-m-d" ,  strtotime(trim($request->input('created_at'))) );
            $transaction_query->where('created_at',  '>'  , $date. " 00:00:00" )->where('created_at',  '<'  , $date. " 23:59:59" );
        }

        if ( $request->has('is_success') &&  in_array( $request->input('is_success'), ["success", "fail"]) )
        {
            switch ($request->input('is_success')) {
              case 'success':
                $transaction_query->where('is_success', true);
                break;
              case 'fail':
                  $transaction_query->where('is_success', false);
                break;

              default:
                break;
            }

        }

        // sorting rules
        $field = "id";
        $order = "desc";

        if ($request->has('sort_by')) {
          $sort_by = $request->input('sort_by');

          if($sort_by == 'amount') {
              $field = "amount";
              $order = "asc";
          }elseif($sort_by == '-amount'){
            $field = "amount";
            $order = "desc";
          }

          if($sort_by == 'balance_after') {
            $field = "balance_after";
            $order = "asc";
          }elseif($sort_by == '-balance_after'){
            $field = "balance_after";
            $order = "desc";
          }

          if($sort_by == 'created_at') {
            $field = "created_at";
            $order = "asc";
          }elseif($sort_by == '-created_at'){
            $field = "created_at";
            $order = "desc";
          }
        }

        $transaction_query->orderBy($field, $order);


        return  $transaction_query->paginate(10)->appends(request()->query());

  }
}




 ?>
