<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Auth;
use App\Models\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class CurrentBalanceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view) {
            $current_balance = 0;
            if (Auth::check()) {
              try {
                $userData = UserData::where('user_id', Auth::id())->first();
                $current_balance = $userData->current_balance;
              } catch (\Exception $e) {

              }
            }

           view()->share('current_balance',$current_balance);
        });
    }
}
