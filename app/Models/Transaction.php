<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Transaction extends Model
{
    use HasFactory;

    public function operation_user_rel()
     {
        return $this->hasOne(User::class, 'id', 'operation_user');
     }

    public function user_rel()
     {
        return $this->hasOne(User::class, 'id', 'user_id');
     }

    public static function generate_request_uid()
     {
        return Str::uuid();
     }
}
