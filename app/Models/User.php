<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;


    public static $user_status = [
        0 => 'inactive',
        1 => 'active',
        2 => 'blocked',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function userdata__rel()
     {
        return $this->hasOne(UserData::class, 'user_id', 'id');
     }





    public static function get_roles_list()
     {
       return ["admin" , "user" , 'manager'];
     }


    public function is_banned()
     {
        $status = $this::$user_status[$this->status];
        if( $status == 'blocked') {
          return true;
        }
        return false;
     }

    public function get_status()
     {
        return $this::$user_status[$this->status];

     }



}
