<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\UserData;
use Illuminate\Support\Facades\Session;

class InitUserBalanceAfterRegistration
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $user_id = $event->user->id;
        $user_data = UserData::firstOrNew(['user_id' => $user_id]);
        $user_data->user_id = $user_id;
        $user_data->current_balance = ($user_data->current_balance ? $user_data->current_balance: 0) +  config('pw-conf.init_user_balance', 0);
        $user_data->save();

        if($user_data->current_balance > 0  ) {
            Session::flash('message', 'You have received a bonus of '.$user_data->current_balance.' PW');
        }


    }
}
