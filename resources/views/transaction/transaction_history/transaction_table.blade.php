{{ $rows->links() }}
<br>
<hr>
<br>

{{ Form::open(array('url' => route('transaction_history') ,  'method' => 'get')) }}

  <table class="w-full text-left rounded-lg">
      <thead>
          <tr class="text-gray-800 border border-b-0">
              <th class="px-4 py-3">ID</th>
              <th class="px-4 py-3">Date</th>
              <th class="px-4 py-3">Result transfer</th>
              <th class="px-4 py-3">Amount</th>
              <th class="px-4 py-3">Balance after operation</th>
              <th class="px-4 py-3">Operation</th>
              <th class="px-4 py-3">Operation User</th>
              <th class="px-4 py-3">Action</th>
          </tr>
      </thead>
      <thead>
          <tr class="text-gray-800 border border-b-0">
              <th class="px-4 py-3"></th>
              <th class="px-4 py-3">
                {{ Form::date('created_at', $request->input('created_at') , array('class' => 'form-control' ,'onchange'=>'this.form.submit()'  )) }}

                @if ($request->input('sort_by') == '-created_at' )
                  <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
                  <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'created_at']) }}"> &uarr; </a>
                @elseif($request->input('sort_by') == 'created_at' )
                  <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-created_at']) }}"> &darr;</a>
                  <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
                @else
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-created_at']) }}"> &darr;</a>
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'created_at']) }}"> &uarr; </a>
                @endif
               </th>

              <th class="px-4 py-3">
                <select class="form-control" name="is_success" class="form-control" onchange="this.form.submit()" style="width:130px">
                  <option value="" {{ (  $request->input('is_success') ==  '') ? 'selected' : '' }}> -- All result --</option>

                    @foreach ([ "success", "fail"] as $key => $value)
                      <option value="{{ $value }}" {{  (strlen($request->input('is_success') >0 ) &&  $value == $request->input('is_success')) ? 'selected' : '' }}>
                          {{ $value }}
                      </option>
                    @endforeach

                </select>
              </th>

              <th class="px-4 py-3">
                  {{ Form::number('amount' , $request->input('amount'), array('class' => 'form-control' , 'style'=>'width:110px' , 'placeholder' => "Amount" ,  'onchange'=>'this.form.submit()'  )) }}

                  @if ($request->input('sort_by') == '-amount' )
                    <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'amount']) }}"> &uarr; </a>
                  @elseif($request->input('sort_by') == 'amount' )
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-amount']) }}"> &darr;</a>
                    <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
                  @else
                      <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-amount']) }}"> &darr;</a>
                      <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'amount']) }}"> &uarr; </a>
                  @endif
              </th>

              <th class="px-4 py-3">
                  {{ Form::number('balance_after' , $request->input('balance_after'), array('class' => 'form-control' , 'style'=>'width:110px' , 'placeholder' => "Balance" ,  'onchange'=>'this.form.submit()'  )) }}

                  @if ($request->input('sort_by') == '-balance_after' )
                    <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'balance_after']) }}"> &uarr; </a>
                  @elseif($request->input('sort_by') == 'balance_after' )
                    <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-balance_after']) }}"> &darr;</a>
                    <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
                  @else
                      <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-balance_after']) }}"> &darr;</a>
                      <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'balance_after']) }}"> &uarr; </a>
                  @endif
              </th>

              <th class="px-4 py-3">
                <select class="form-control" name="operation_type" class="form-control" onchange="this.form.submit()" style="width:130px">
                  <option value="" {{ (  $request->input('operation_type') ==  '') ? 'selected' : '' }}> -- All type operation --</option>

                    @foreach ([ "debiting", "crediting"] as $key => $value)
                      <option value="{{ $value }}" {{  (strlen($request->input('operation_type') >0 ) &&  $value == $request->input('operation_type')) ? 'selected' : '' }}>
                          {{ $value }}
                      </option>
                    @endforeach

                </select>
              </th>

              <th class="px-4 py-3">
                 {{ Form::text('operation_user' , $request->input('operation_user'), array('class' => 'form-control' ,  'placeholder' => "User" , 'onchange'=>'this.form.submit()'  )) }}
               </th>
              <th class="px-4 py-3"></th>
          </tr>
      </thead>
      <tbody>
        @foreach ($rows as $row)
          <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
              <td class="px-4 py-4">{{$row->id}}</td>
              <td class="px-4 py-4">{{$row->created_at}}</td>
              <td class="px-4 py-4">
                @if ($row->is_success === true)
                  <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded-full">Success</span>
                @else
                  <span class="bg-red-500 text-gray-200 text-xs px-2 py-1 rounded-full">Fail</span>
                @endif
              </td>
              <td class="px-4 py-4">{{$row->amount}} PW</td>
              <td class="px-4 py-4"> {{$row->balance_after}} PW</td>

              <td class="px-4 py-4">
                @if ($row->operation_type === 'debiting')
                   <span style="color:red">{{$row->operation_type}}  &#8594; </span>
                @else
                  <span style="color:green">  &#8592;	 {{$row->operation_type}}</span>
                @endif
              </td>
              <td class="px-4 py-4"> {{$row->operation_user_rel->name}}</td>
              <td class="px-4 py-4">
                @if ($row->operation_type == 'debiting' )
                    <a href="{{route("transfer_copy" , ["transaction_id" => $row->id ])}}" class="bg-gray-600 text-gray-100 rounded hover:bg-gray-500 px-4 py-2 focus:outline-none">Copy</a>
                @else

                @endif

              </td>
          </tr>
        @endforeach

      </tbody>
  </table>
{{ Form::close() }}
