@push('scripts')
    <script src="/js/autocomplete_name.js"></script>
@endpush


<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                      <div class="bg-white rounded-lg shadow-lg py-6">
                        <div class="block overflow-x-auto mx-6">

                            @if ($errors->any())
                              <div class="bg-red-200 relative text-red-500 py-3 px-3 rounded-lg">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                              <br>
                            @endif

                            {{ Form::open(array('url' => route('transfering'), 'method' => 'post')) }}


                                  <label class="text-gray-600 font-light">Transfer amount (PW)</label>
                                  {{ Form::text('transfer_amount', $request->old('transfer_amount'), array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500' ,  'pattern' => "^\d*(\.\d{0,2})?$",  'placeholder' => "10.00", "required" => "required"  )) }}

                                  <label class="text-gray-600 font-light">User who needs to transfer money</label>


                                  {{ Form::text('operation_user', $request->old('operation_user'), array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500' ,  'placeholder' => "Username",  "required" => "required"   , "autocomplete"=>"off"  )) }}

                                  {{ Form::hidden('operation_user_id', $request->old('operation_user_id') ) }}


                                  {{ Form::hidden('request_uid',  $request->old('request_uid') ? $request->old('request_uid'):   $request_uid ) }}


                                  <ul class="px-0" id="suggestion-name">
                                  </ul>
                                  <br>


                                  {{Form::submit('Transfer now',  array('class' => 'bg-green-500 text-gray-100 rounded hover:bg-green-400 px-4 py-2 focus:outline-none'))}}

                            {{ Form::close() }}

                        </div>
                      </div>

                </div>
            </div>
        </div>
    </div>

    <style>
    #suggestion-name{
        position: absolute;
        background-color: #f9f9f9;
        width: 300px;
        margin-top: -26px;
    }
    </style>






</x-app-layout>
