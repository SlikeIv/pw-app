<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Result transfer</h2>

                    <div class="bg-white rounded-lg shadow-lg py-6">
                      <div class="block overflow-x-auto mx-6">
                          <table class="w-full text-left rounded-lg">

                              <tbody>
                                  <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                      <td class="px-4 py-4">Status transfer</td>
                                      <td class="px-4 py-4">
                                        @if ($transaction->is_success === true)
                                            <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded-full">Success</span>
                                        @else
                                            <span class="bg-red-400 text-gray-100 text-xs px-2 py-1 rounded-full">Fail</span>
                                        @endif
                                      </td>
                                  </tr>
                                  <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                      <td class="px-4 py-4">Amount</td>
                                      <td class="px-4 py-4">{{$transaction->amount}} PW</td>
                                  </tr>
                                  <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                      <td class="px-4 py-4">Current balance</td>
                                      <td class="px-4 py-4">{{$current_balance}} PW</td>
                                  </tr>
                                  <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                      <td class="px-4 py-4">Funds transferred to the user</td>
                                      <td class="px-4 py-4">{{$transaction->operation_user_rel->name}}</td>
                                  </tr>

                              </tbody>
                          </table>
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </div>





</x-app-layout>
