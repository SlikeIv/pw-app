<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                  <a href="{{route("user.edit",  $user->id)}}" class="bg-green-500 text-gray-100 rounded hover:bg-green-400 px-4 py-2 focus:outline-none">Edit User</a>
                  <br>
                  <br>
                  <hr>

                  <div class="bg-white rounded-lg shadow-lg py-6">
                    <div class="block overflow-x-auto mx-6">
                        <table class="w-full text-left rounded-lg">
                            <tbody>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">ID user</td>
                                    <td class="px-4 py-4">{{ $user->id }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Name</td>
                                    <td class="px-4 py-4">{{ $user->name }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">E-Mail</td>
                                    <td class="px-4 py-4">{{ $user->email }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Role</td>
                                    <td class="px-4 py-4">{{ $user->role }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Status</td>
                                    <td class="px-4 py-4">

                                      @switch( $user->get_status())
                                          @case("inactive")
                                              <span style="color:orange">inactive</span>
                                              @break

                                          @case("active")
                                              <span style="color:green">active</span>
                                              @break
                                          @case("blocked")
                                              <span style="color:red">blocked</span>
                                              @break
                                          @default
                                              <span>{{$user->status}}</span>
                                      @endswitch

                                    </td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Balance</td>
                                    <td class="px-4 py-4">

                                      @if(empty( $user->userdata__rel))
                                             NULL
                                          @else
                                            {{$user->userdata__rel->current_balance}} PW
                                          @endif

                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
