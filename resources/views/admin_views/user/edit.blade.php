<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                      <div class="bg-white rounded-lg shadow-lg py-6">
                        <div class="block overflow-x-auto mx-6">

                          @if ($errors->any())
                          <div class="bg-red-200 relative text-red-500 py-3 px-3 rounded-lg">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                            </div>
                            <br>
                          @endif

                          <form method="post" action="{{route('user.update' , ["user"=>$user->id]) }}">
                            @method('PATCH')
                            @csrf

                              <label class="text-gray-600 font-light">E-mail:</label>
                              {{ Form::text('email', $user->email, array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500',  "required" => "required"  )) }}


                              <label class="text-gray-600 font-light">Name:</label>
                              {{ Form::text('name',  $user->name   , array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500',  "required" => "required"  )) }}

                              <label class="text-gray-600 font-light">Status:</label>
                              {{Form::select('status', $statuses,  $user->status , array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500',  "required" => "required"  )) }}


                              <label class="text-gray-600 font-light">Role:</label>
                              <div class="flex">
                                @foreach ($roles as $role)
                                  <div class="flex items-center mb-2 mr-4">
                                      {!! Form::radio('role', $role , isset( $user->role) ? $user->role === $role : false,   array('class' => 'h-4 w-4 text-gray-700 px-3 py-3 border rounded mr-2')) !!}
                                      <label for="radio-example-1" class="text-gray-600">{{$role}}</label>
                                  </div>

                                @endforeach
                              </div>


                              <label class="text-gray-600 font-light">Password:</label>
                              {{Form::password('password',   array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500' )) }}


                              <label class="text-gray-600 font-light">Confirm password:</label>
                              {{Form::password('password_confirm',  array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500' )) }}


                              <br>

                              {{Form::submit('Update',  array('class' => 'bg-green-500 text-gray-100 rounded hover:bg-green-400 px-4 py-2 focus:outline-none'))}}

                          </form>


                        </div>
                      </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
