<br>

{{ Form::open(array('url' => route('user.index') ,  'method' => 'get')) }}
  @csrf

  {{ $users->links() }}
  <br>
  <hr>
  <br>

  <table class="w-full text-left rounded-lg">
      <thead>
          <tr class="text-gray-800 border border-b-0">
              <th class="px-4 py-3">ID</th>
              <th class="px-4 py-3">Name</th>
              <th class="px-4 py-3">E-mail</th>
              <th class="px-4 py-3">Status</th>
              <th class="px-4 py-3">Role</th>
              <th class="px-4 py-3">Registration date</th>
              <th class="px-4 py-3">Actions</th>
          </tr>
      </thead>
      <thead>
        <tr class="text-gray-800 border border-b-0">
          <th class="px-4 py-3">
            {{ Form::number('id' , $request->input('id'), array('class' => 'form-control' , 'style'=>'width:80px' , 'placeholder' => "ID" , 'onchange'=>'this.form.submit()')) }}
            @if ($request->input('sort_by') == '-id' )
              <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
              <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'id']) }}"> &uarr; </a>
            @elseif($request->input('sort_by') == 'id' )
              <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-id']) }}"> &darr;</a>
              <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
            @else
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-id']) }}"> &darr;</a>
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'id']) }}"> &uarr; </a>
            @endif
          </th>
          <th class="px-4 py-3">
              {{ Form::text('name' , $request->input('name'), array('class' => 'form-control' ,  'style'=>'width:150px', 'placeholder' => "Name", 'onchange'=>'this.form.submit()')) }}
          </th>
          <th class="px-4 py-3">
               {{ Form::text('email' , $request->input('email'), array('class' => 'form-control' ,  'style'=>'width:150px',  'placeholder' => "E-mail " , 'onchange'=>'this.form.submit()')) }}
          </th>
          <th class="px-4 py-3">
            {{ Form::select('status', array_merge(['' => "-- all status --" ] , $statuses), $request->input('status') , array('class' => 'form-control' , 'onchange'=>'this.form.submit()' , 'style'=>'width:140px'))}}

            @if ($request->input('sort_by') == '-status' )
              <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
              <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'status']) }}"> &uarr; </a>
            @elseif($request->input('sort_by') == 'status' )
              <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-status']) }}"> &darr;</a>
              <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
            @else
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-status']) }}"> &darr;</a>
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'status']) }}"> &uarr; </a>
            @endif

          </th>
          <th class="px-4 py-3">
            {{ Form::select('role', array_combine(array_merge([ "" ] , $roles), array_merge([ "-- all roles --" ] , $roles)), $request->input('role') , array('class' => 'form-control' , 'onchange'=>'this.form.submit()' , 'style'=>'width:120px'))}}
          </th>
          <th class="px-4 py-3">
              Start date: {{ Form::date('date_start' , $request->input('date_start'), array('class' => 'form-control' , 'style'=>'width:150px' ,  'placeholder' => "Date start" , 'onchange'=>'this.form.submit()')) }}
              <br>
              End date: {{ Form::date('date_end' , $request->input('date_end'), array('class' => 'form-control', 'style'=>'width:150px' ,  'placeholder' => "Date end" , 'onchange'=>'this.form.submit()')) }}

              @if ($request->input('sort_by') == '-created_at' )
                <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded">&darr;</span>
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'created_at']) }}"> &uarr; </a>
              @elseif($request->input('sort_by') == 'created_at' )
                <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-created_at']) }}"> &darr;</a>
                <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded"> &uarr;</span>
              @else
                  <a href="{{ request()->fullUrlWithQuery(['sort_by' => '-created_at']) }}"> &darr;</a>
                  <a href="{{ request()->fullUrlWithQuery(['sort_by' => 'created_at']) }}"> &uarr; </a>
              @endif
          </th>
          <th class="px-4 py-3"></th>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $user)
        <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
          <td class="px-4 py-4">{{$user->id}}</td>
          <td class="px-4 py-4">{{$user->name}}</td>
          <td class="px-4 py-4">{{$user->email}}</td>
          <td class="px-4 py-4">
            @switch( $user->get_status())
                @case("inactive")
                    <span style="color:orange">inactive</span>
                    @break

                @case("active")
                    <span style="color:green">active</span>
                    @break
                @case("blocked")
                    <span style="color:red">blocked</span>
                    @break
                @default
                    <span>{{$user->status}}</span>
            @endswitch
          </td>
          <td class="px-4 py-4">{{$user->role}}</td>
          <td class="px-4 py-4">{{ date('d.m.Y H:i:s', strtotime($user->created_at))}}</td>
          <td class="px-4 py-4">
            <a href="{{ route('user.edit', $user->id)}}" class="bg-gray-600 text-gray-100 text-xs px-2 py-1 rounded-full">Edit</a>
            <a href="{{ route('user.show', $user->id)}}" class="bg-blue-600 text-gray-200 text-xs px-2 py-1 rounded-full">View</a>

          </td>
        </tr>
        @endforeach

      </tbody>
    </table>



{{ Form::close() }}
