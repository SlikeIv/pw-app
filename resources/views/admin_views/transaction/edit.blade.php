<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                      <div class="bg-white rounded-lg shadow-lg py-6">
                        <div class="block overflow-x-auto mx-6">

                          @if ($errors->any())
                          <div class="bg-red-200 relative text-red-500 py-3 px-3 rounded-lg">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                            </div>
                            <br>
                          @endif

                          <form method="post" action="{{route('transaction.update' , ["transaction"=>$transaction->id]) }}">
                            @method('PATCH')
                            @csrf

                              <label class="text-gray-600 font-light">Amount:</label>
                              {{ Form::text('amount', $transaction->amount, array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500',  "required" => "required"  )) }}

                              <label class="text-gray-600 font-light">Fail message:</label>
                              {{ Form::textarea('fail_message',  $transaction->fail_message   , array('class' => 'w-full mt-2 mb-6 px-4 py-2 border rounded-lg text-gray-700 focus:outline-none focus:border-green-500' )) }}



                              <br>

                              {{Form::submit('Update',  array('class' => 'bg-green-500 text-gray-100 rounded hover:bg-green-400 px-4 py-2 focus:outline-none'))}}

                          </form>


                        </div>
                      </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
