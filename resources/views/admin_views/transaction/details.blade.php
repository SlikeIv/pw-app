<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                  <a href="{{route("transaction.edit",  $transaction->id)}}" class="bg-green-500 text-gray-100 rounded hover:bg-green-400 px-4 py-2 focus:outline-none">Edit Transaction</a>
                  <br>
                  <br>
                  <hr>

                  <div class="bg-white rounded-lg shadow-lg py-6">
                    <div class="block overflow-x-auto mx-6">
                        <table class="w-full text-left rounded-lg">
                            <tbody>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">ID Transaction</td>
                                    <td class="px-4 py-4">{{ $transaction->id }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">User</td>
                                    <td class="px-4 py-4"> <a href="{{route("user.show", $transaction->user_rel->id )}}"> {{ $transaction->user_rel->name }}  (id: {{ $transaction->user_rel->id }}) </a></td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Date\Time</td>
                                    <td class="px-4 py-4">{{ $transaction->created_at }}</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Amount</td>
                                    <td class="px-4 py-4">{{ $transaction->amount }} PW</td>
                                </tr>
                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">type operation</td>
                                    <td class="px-4 py-4">{{ $transaction->operation_type }}</td>
                                </tr>

                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Operation User</td>
                                    <td class="px-4 py-4"> <a href="{{route("user.show", $transaction->operation_user_rel->id )}}"> {{$transaction->operation_user_rel->name}} (id: {{ $transaction->operation_user_rel->id }}) </a></td>
                                </tr>

                                <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                    <td class="px-4 py-4">Result</td>
                                    <td class="px-4 py-4">  @if ($transaction->is_success === true)
                                        <span class="bg-green-400 text-gray-100 text-xs px-2 py-1 rounded-full">Success</span>
                                      @else
                                        <span class="bg-red-500 text-gray-200 text-xs px-2 py-1 rounded-full">Fail</span>
                                      @endif
                                    </td>
                                </tr>
                                @if ($transaction->fail_message)
                                  <tr class="w-full font-light text-gray-700 bg-gray-100 whitespace-no-wrap border border-b-0">
                                      <td class="px-4 py-4">Fail message</td>
                                      <td class="px-4 py-4">

                                          <span class="bg-red-500 text-gray-200 text-xs px-2 py-1 rounded-full">{{ $transaction->fail_message }}</span>

                                      </td>
                                  </tr>
                                @else

                                @endif




                            </tbody>
                        </table>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
