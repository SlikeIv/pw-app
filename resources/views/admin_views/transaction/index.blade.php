<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>



    <div class="py-12">
        <div class="max-w-9xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                      <h1 class="text-5xl font-normal leading-normal mt-0 mb-2 text-pink-800">Transactions</h1>

                      <div class="bg-white rounded-lg shadow-lg py-6">
                        <div class="block overflow-x-auto mx-6">


                            @include('admin_views.transaction._transaction_table')


                        </div>
                      </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
